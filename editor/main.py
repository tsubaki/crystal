import curses
import pyglet
import threading
import time

colors = [curses.COLOR_BLACK, curses.COLOR_WHITE, curses.COLOR_RED, curses.COLOR_GREEN, curses.COLOR_BLUE, curses.COLOR_YELLOW, curses.COLOR_CYAN, curses.COLOR_MAGENTA]
color_pairs = {}
breathless_path = 'Assets/Resources/GENERATED_CHART.txt'
mp3_path = 'Assets/Resources/Ichido Dake no Koi nara.mp3'


class Song:
    def __init__(self):
        self.events = []
        self.bpm = 172
        self.offset = 0.153
        self.music = pyglet.media.load(mp3_path)
        self.player = pyglet.media.Player()
        self.player.queue(self.music)

    def toggle_beat(self, beat_time, key):
        l = len(self.events)
        self.events = [x for x in self.events if not (isinstance(x, Beat) and x.beat_time == beat_time and x.key == key)]
        if len(self.events) == l:
            self.events.append(Beat(beat_time, key))

    def __str__(self):
        self.events.sort(key=lambda x: x.beat_time)
        return 'BPM %d\nOFFSET %.4f\n\n' % (self.bpm, self.offset) + '\n'.join(str(x) for x in self.events)

    def from_string(string):
        song = Song()
        commands = [x.strip() for x in string.split('\n')]
        for command in commands:
            if command:
                words = command.split()
                if words[0] == 'BPM':
                    song.bpm = int(words[1])
                elif words[0] == 'OFFSET':
                    song.offset = float(words[1])
                elif words[0] == 'SPEED':
                    pass
                else:
                    song.events.append(Beat(float(words[0]), 'jklsdf'[int(words[1])]))
        return song

    def save(self):
        open(breathless_path, 'w').write(str(self))


class Event:
    def __init__(self, beat_time):
        self.beat_time = beat_time


class Beat(Event):
    def __init__(self, beat_time, key):
        super().__init__(beat_time)
        self.key = key

    def num(self):
        return 'jklsdf'.index(self.key)

    def __str__(self):
        return '%.4f %d' % (self.beat_time, self.num())


class EventsView:
    def __init__(self, window, song=None, top=0, beats_per_line=0.25):
        self.window = window
        self.song = song
        self.top = top
        self._insertion_point = top  # insertion point
        self._beats_per_line = beats_per_line
        self.selection = None

    def play(self):
        self.song.player.seek(self.current_second)
        self.song.player.play()
        while self.song.player.playing:
            time.sleep(0.01)
            t = self.song.player.time
            beat = (t - self.song.offset) * self.song.bpm / 60
            beat -= beat % self.beats_per_line
            insertion_point = beat
            if insertion_point != self._insertion_point:
                self.insertion_point = insertion_point

    @property
    def current_second(self):
        return 60 * self._insertion_point / self.song.bpm + self.song.offset

    @property
    def bottom(self):
        return self.top + self.beats_per_line * (self.window.getmaxyx()[0] - 1)

    @bottom.setter
    def bottom(self, value):
        self.top = value - self.beats_per_line * (self.window.getmaxyx()[0] - 1)

    @property
    def insertion_point(self):
        return self._insertion_point

    @property
    def beats_per_line(self):
        return self._beats_per_line

    @beats_per_line.setter
    def beats_per_line(self, value):
        self.insertion_point -= self.insertion_point % value
        old_value = self._beats_per_line
        self._beats_per_line = value
        self.top = self._insertion_point + (self.top - self._insertion_point) * value / old_value

    @insertion_point.setter
    def insertion_point(self, value):
        self._insertion_point = value
        if value < self.top:
            self.top = value
        elif value > self.bottom:
            self.bottom = value

    def render(self):
        rows, cols = self.window.getmaxyx()
        for row in range(rows):
            beat_time = self.top + row * self.beats_per_line
            if beat_time == self.insertion_point:
                bgcolor = curses.COLOR_RED
            elif self.selection and self.selection[0] <= beat_time <= self.selection[1]:
                bgcolor = curses.COLOR_MAGENTA
            else:
                bgcolor = curses.COLOR_BLACK
            self.window.addnstr(row, 0, self.format_beat_time(beat_time), 8, color_pairs[(curses.COLOR_BLUE, bgcolor)])
            beat_nums = {event.num() for event in self.song.events if isinstance(event, Beat) and event.beat_time == beat_time}
            for beat_num in range(6):
                symbol = '    ' + 'JKLSDF'[beat_num] + '    '
                beat_position = (beat_num + 3) % 6
                if beat_position >= 3:
                    beat_position += 1
                if beat_num in beat_nums:
                    self.window.addnstr(row, 10 * beat_position + 10, symbol, 9, color_pairs[(curses.COLOR_BLUE, curses.COLOR_WHITE)])
                else:
                    self.window.addnstr(row, 10 * beat_position + 10, symbol, 9, color_pairs[(curses.COLOR_BLACK, bgcolor)])

    def poll(self):
        key = self.window.getch()
        if key in {curses.KEY_UP, curses.KEY_SR}:
            self.insertion_point -= self.beats_per_line
        elif key in {curses.KEY_DOWN, curses.KEY_SF}:
            self.insertion_point += self.beats_per_line
        elif key == ord(' '):
            self.insertion_point += self.beats_per_line * self.window.getmaxyx()[0]
        elif key == ord('-'):
            self.beats_per_line *= 2
        elif key in {ord('+'), ord('=')}:
            self.beats_per_line /= 2
        elif key in {ord(x) for x in 'jklsdf'}:
            self.song.toggle_beat(self.insertion_point, chr(key))
        elif key == ord('`'):
            self.song.save()
        elif key == ord('p'):
            if self.song.player.playing:
                self.song.player.pause()
            else:
                thread = threading.Thread(target=self.play)
                thread.start()
        elif key == ord('q'):
            self.insertion_point = 0
        elif key in {curses.KEY_BACKSPACE, 127}:
            self.song.events = [x for x in self.song.events if x.beat_time != self.insertion_point]
            if self.selection:
                self.song.events = [x for x in self.song.events if x.beat_time < self.selection[0] or x.beat_time > self.selection[1]]
        # do selections
        if key == curses.KEY_SR:
            if self.selection:
                self.selection[0] = self.insertion_point
            else:
                self.selection = [self.insertion_point, self.insertion_point + self.beats_per_line]
        elif key == curses.KEY_SF:
            if self.selection:
                self.selection[1] = self.insertion_point
            else:
                self.selection = [self.insertion_point - self.beats_per_line, self.insertion_point]
        elif key != -1:
            self.selection = None

    def format_beat_time(self, beat):
        beat = str(beat)
        if '.' not in beat:
            beat = (beat + '    ').rjust(8)
        else:
            whole, frac = beat.split('.')
            frac = frac.ljust(3)
            whole = whole.rjust(4)
            return whole + '.' + frac


def init_colors():
    i = 0
    for foreground in colors:
        for background in colors:
            i += 1
            curses.init_pair(i, foreground, background)
            color_pairs[(foreground, background)] = curses.color_pair(i)


def main(screen):
    rows, cols = screen.getmaxyx()
    events_window = curses.newwin(rows, cols, 0, 0)
    events_window.nodelay(1)
    events_window.keypad(1)
    curses.curs_set(0)
    curses.start_color()
    curses.use_default_colors()
    init_colors()
    song = Song.from_string(open(breathless_path).read())
    events_view = EventsView(events_window, song=song)
    while True:
        time.sleep(0.01)
        try:
            events_view.render()
            events_view.poll()
        except KeyboardInterrupt:
            return


if __name__ == '__main__':
    curses.wrapper(main)
