﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatController : MonoBehaviour {

	public float speed;

	private static BeatPool beatPool;
	private static AudioSource audioSource;

	private float startTime;
	private float sampleRateReciprocal;

	private bool isHit;
	private Animator anim;
	private static int acc0HitTriggerHash = Animator.StringToHash ("acc0HitTrigger");
	private static int acc1HitTriggerHash = Animator.StringToHash ("acc1HitTrigger");
	private static int acc2HitTriggerHash = Animator.StringToHash ("acc2HitTrigger");
	private static int acc3HitTriggerHash = Animator.StringToHash ("acc3HitTrigger");
	private static int missTriggerHash = Animator.StringToHash ("missTrigger");

	public static void Attach (BeatPool beatPool, AudioSource audioSource) {
		BeatController.beatPool = beatPool;
		BeatController.audioSource = audioSource;
	}

	void Update () {
		if (isHit) {
			return;
		}

		transform.localPosition = new Vector3 (0, speed * anim.speed *
			(BeatController.audioSource.timeSamples * sampleRateReciprocal - startTime), 1);
	}

	public void Fire (Transform beatbarTransform, float startTime, float multiplier) {
		if (anim == null) {
			anim = GetComponent<Animator> ();
		}
		anim.speed = multiplier;
		this.startTime = startTime;

		sampleRateReciprocal = 1.0F / BeatController.audioSource.clip.frequency;

		transform.SetParent (beatbarTransform.parent);
		transform.rotation = beatbarTransform.rotation;
		transform.localPosition = new Vector3 (0, speed * anim.speed *
			(BeatController.audioSource.timeSamples * sampleRateReciprocal - startTime), 1);

		isHit = false;
		this.gameObject.SetActive (true);
	}

	public void Return (int accuracy) {
		Update ();
		isHit = true;
		anim.speed = 1;
		if (accuracy == 0) {
			anim.SetTrigger (acc0HitTriggerHash);
		} else if (accuracy == 1) {
			anim.SetTrigger (acc1HitTriggerHash);
		} else if (accuracy == 2) {
			anim.SetTrigger (acc2HitTriggerHash);
		} else if (accuracy == 3) {
			anim.SetTrigger (acc3HitTriggerHash);
		} else {
			anim.SetTrigger (missTriggerHash);
		}
	}

	void ReturnToPool () {
		this.gameObject.SetActive (false);
		beatPool.Return (this);
	}

//	void OnBecameInvisible () {
//		if (this.gameObject.activeInHierarchy) {
//			ReturnToPool ();
//		}
//	}
}
