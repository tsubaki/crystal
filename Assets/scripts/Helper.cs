﻿using UnityEngine;

class Helper {
	private static string[] anyButtons = new string[] {"Hit0", "Hit1", "Hit2", "Hit3", "Hit4", "Hit5", "Submit"};

	public static float mod (float a, float b) {
		float tmp = a % b;
		return (tmp < 0) ? tmp + b : tmp;
	}

	public static bool GetAnyButton () {
		foreach (string button in anyButtons) {
			if (Input.GetButtonDown (button)) {
				return true;
			}
		}
		return false;
	}
}
