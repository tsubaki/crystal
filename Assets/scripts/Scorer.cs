﻿using UnityEngine;

class Scorer {
	private static int ACC_0_VALUE = 2;
	private static int ACC_1_VALUE = 1;
	private static int ACC_2_VALUE = 0;
	private static int ACC_3_VALUE = 0;
	private static int MAX_SCORE = 1000000;
	private static float COMBO_WEIGHT = 0.1F;
	private static int COMBO_BONUS_PERIOD = 5;

	public int acc0Count { get; private set; }
	public int acc1Count { get; private set; }
	public int acc2Count { get; private set; }
	public int acc3Count { get; private set; }
	public int missCount { get; private set; }
	public int comboCount { get; private set; }

	private int score;
	private int comboScore;
	private int beatCount;

	public void Reset () {
		acc0Count = 0;
		acc1Count = 0;
		acc2Count = 0;
		acc3Count = 0;
		missCount = 0;
		comboCount = 0;
		score = 0;
		comboScore = 0;
		beatCount = 0;
	}

	public void SetBeatCount (int count) {
		beatCount = count;
	}

	public int NormalizedScore {
		get {
			Debug.Assert (beatCount > 0);
			return Mathf.RoundToInt ((
				(1 - COMBO_WEIGHT) * score / (ACC_0_VALUE * beatCount) +
				COMBO_WEIGHT * comboScore / (beatCount / COMBO_BONUS_PERIOD)) * MAX_SCORE);
		}
	}

	public string Grade {
		get {
			float fraction = (float) NormalizedScore / MAX_SCORE;
			if (fraction >= 0.95) {
				return "CRYSTAL";
			} else if (fraction >= 0.9) {
				return "X";
			} else if (fraction >= 0.8) {
				return "S";
			} else if (fraction >= 0.7) {
				return "A";
			} else if (fraction >= 0.6) {
				return "B";
			} else if (fraction >= 0.5) {
				return "C";
			} else if (fraction >= 0.4) {
				return "D";
			} else {
				return "SHATTER";
			}
		}
	}

	public void HitAcc0 () {
		++acc0Count;
		score += ACC_0_VALUE;
		Combo ();
	}

	public void HitAcc1 () {
		++acc1Count;
		score += ACC_1_VALUE;
		Combo ();
	}

	public void HitAcc2 () {
		++acc2Count;
		score += ACC_2_VALUE;
		Combo ();
	}

	public void HitAcc3 () {
		++acc3Count;
		score += ACC_3_VALUE;
		comboCount = 0;
	}

	public void Miss () {
		++missCount;
		comboCount = 0;
	}

	void Combo () {
		if (++comboCount % COMBO_BONUS_PERIOD == 0) {
			++comboScore;
		}
	}
}
