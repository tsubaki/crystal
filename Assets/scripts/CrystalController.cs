﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class CrystalController : MonoBehaviour {

	public GameObject beatPrefab;
	public BeatbarController[] beatbars;
	public AudioSource audioSource;
//	public Text hitFeedbackText;
	public Text nowPlayingText;
	public Text scoreText;
	public Text comboText;

	public float rotationSpeed;

	public float acc0Threshold;
	public float acc1Threshold;
	public float acc2Threshold;
	public float acc3Threshold;
	public float missThreshold;

	private Animator anim;

	private BeatPool beatPool;
	private BeatQueue beatChart;
	private BeatQueue[] firedBeats;
	private float sampleRateReciprocal;
	private float time;
	private float lastTime;
	private float bps;
	private float offset;

	private Scorer scorer;
	private static int MIN_COMBO = 3;
	private bool shouldUpdateScore;

	#if UNITY_EDITOR
	public string editorSongName;
	public bool CMMode;
	public float CMBPM;
	public float CMOffset;
	public float CMResolution;

	private static string GENERATED_CHART_BASENAME = "GENERATED_CHART";
	private static string GENERATED_CHART_PATH;
	#endif

	void Awake () {
		#if UNITY_EDITOR
		GENERATED_CHART_PATH = UnityEditor.AssetDatabase.GetAssetPath (
			Resources.Load (GENERATED_CHART_BASENAME, typeof(TextAsset)));
		#endif

		Debug.Assert (acc0Threshold < acc1Threshold);
		Debug.Assert (acc1Threshold < acc2Threshold);
		Debug.Assert (acc2Threshold < acc3Threshold);
		Debug.Assert (acc3Threshold < missThreshold);

		anim = GetComponent<Animator> ();

		beatPool = new BeatPool(beatPrefab, transform);
		BeatController.Attach (beatPool, audioSource);

		beatChart = new BeatQueue (code => code.startTime);
		firedBeats = new BeatQueue[6];
		for (int i = 0; i < 6; ++i) {
			firedBeats [i] = new BeatQueue (code => code.time);
		}

		scorer = new Scorer ();
	}

	void OnEnable () {
		#if UNITY_EDITOR
		if (CMMode) {
			Debug.Assert (CMBPM > 0);
			Debug.Assert (CMOffset >= 0);
			Debug.Assert (CMResolution > 0);
			Play (editorSongName);
			bps = CMBPM / 60;
			offset = CMOffset;
			File.WriteAllText (GENERATED_CHART_PATH, string.Format (
				"BPM {0}\nOFFSET {1}\n\n", CMBPM, CMOffset));
		} else {
			Play (GENERATED_CHART_BASENAME, editorSongName);
		}
		#else
		Play (Manager.currSongAssetName);
		#endif
	}

	void Update () {
		if (!audioSource.isPlaying) {
			#if UNITY_EDITOR
			if (CMMode) {
				Application.Quit ();
			}
			#endif
			Manager.FinishSong (nowPlayingText.text, scorer);
			return;
		}

		time = audioSource.timeSamples * sampleRateReciprocal;

		transform.Rotate (new Vector3 (0, 0, rotationSpeed * bps * (time - lastTime)));

		// fire beats
		BeatCode nextBeatCode;
		while (beatChart.Count > 0) {
			nextBeatCode = beatChart.Peek ();
			if (time > nextBeatCode.startTime) {
				beatChart.Dequeue ();
				nextBeatCode.controller = beatPool.Borrow ();
				nextBeatCode.controller.Fire (
					beatbars [nextBeatCode.barId].transform,
					nextBeatCode.startTime,
					nextBeatCode.speed);
				firedBeats [nextBeatCode.barId].Enqueue (nextBeatCode);
			} else {
				break;
			}
		}

		// expire beats
		for (int i = 0; i < 6; ++i) {
			BeatCode beatCode;
			while (firedBeats [i].Count > 0) {
				beatCode = firedBeats [i].Peek ();
				if (time - beatCode.time >= missThreshold) {
					firedBeats [i].Dequeue ();
					beatCode.controller.Return (-1);
					scorer.Miss ();
				} else {
					break;
				}
			}
		}
	}

	void LateUpdate () {
		CheckBeatbar ("Hit0", 0);
		CheckBeatbar ("Hit1", 1);
		CheckBeatbar ("Hit2", 2);
		CheckBeatbar ("Hit3", 3);
		CheckBeatbar ("Hit4", 4);
		CheckBeatbar ("Hit5", 5);

		if (shouldUpdateScore) {
//			hitFeedbackText.text = string.Format (
//				"Perfect: {0}\nGreat: {1}\nGood: {2}\nBad: {3}\nMiss: {4}",
//				scorer.acc0Count, scorer.acc1Count, scorer.acc2Count, scorer.acc3Count, scorer.missCount);

			scoreText.text = scorer.NormalizedScore.ToString ();

			if (scorer.comboCount < MIN_COMBO) {
				comboText.text = "";
			} else {
				comboText.text = scorer.comboCount.ToString ();
			}
		}

		lastTime = time;
	}

	void CheckBeatbar (string button, int beatbarId) {
		if (Input.GetButtonDown (button)) {
			if (!Input.GetButtonUp (button)) {
				beatbars [beatbarId].Activate ();
			}

			if (firedBeats [beatbarId].Count > 0) {
				BeatCode beatCode = firedBeats [beatbarId].Peek ();
				float error = Mathf.Abs (time - beatCode.time);
				if (error < missThreshold) {
					firedBeats [beatbarId].Dequeue ();
					if (error < acc0Threshold) {
						beatCode.controller.Return (0);
						scorer.HitAcc0 ();
					} else if (error < acc1Threshold) {
						beatCode.controller.Return (1);
						scorer.HitAcc1 ();
					} else if (error < acc2Threshold) {
						beatCode.controller.Return (2);
						scorer.HitAcc2 ();
					} else if (error < acc3Threshold) {
						beatCode.controller.Return (3);
						scorer.HitAcc3 ();
					} else {
						beatCode.controller.Return (-1);
						scorer.Miss ();
					}
					shouldUpdateScore = true;
				}
			}

			#if UNITY_EDITOR
			if (CMMode) {
				File.AppendAllText (GENERATED_CHART_PATH, string.Format (
					"{0} {1}\n", GetTime (CMBPM, CMOffset, CMResolution), beatbarId));
			}
			#endif
		} else if (Input.GetButtonUp (button)) {
			beatbars [beatbarId].Deactivate ();
		}
	}

	void Play (string chartName, string songName=null) {
		if (songName == null) {
			songName = chartName;
		}

		Reset ();

		if (chartName != null) {
			LoadChart (chartName);
		}

		audioSource.Stop ();
		audioSource.clip = (AudioClip) Resources.Load (songName, typeof(AudioClip));
		audioSource.Play ();
		sampleRateReciprocal = 1.0F / audioSource.clip.frequency;

		// pulsation
		anim.speed = bps;
		anim.Play ("pulsation", -1, Helper.mod (-offset * bps, 1F));

		// rotation
		transform.rotation = Quaternion.identity;
	}

	void Reset () {
		beatChart.Clear ();
		for (int i = 0; i < 6; ++i) {
			firedBeats [i].Clear ();
		}
		lastTime = 0;
		scorer.Reset ();

//		hitFeedbackText.text = "Perfect: 0\nGreat: 0\nGood: 0\nBad: 0\nMiss: 0";
		nowPlayingText.text = "";
		scoreText.text = "0";
		comboText.text = "";
	}

	void LoadChart (string chartName) {
		float bps = -1;
		float offset = -1;
		float multiplier = 1;

		TextAsset asset = (TextAsset)Resources.Load (chartName, typeof(TextAsset));

		string line;
		string[] tokens;
		bool metaFlag = false;
		foreach (string l in asset.text.Split ('\n')) {
			line = l.Trim ();
			if (line.Length == 0 || line [0] == ';') {
				continue;
			}
			tokens = line.Split (' ');

			if (tokens [0] == "===") {
				metaFlag = !metaFlag;
				continue;
			}

			if (metaFlag) {
				if (tokens [0] == "NAME") {
					nowPlayingText.text = string.Join (" ", tokens, 1, tokens.Length - 1);
				} else {
					Debug.Assert (false, "Unrecognized chart meta keyword");
				}
			} else {
				if (tokens [0] == "BPM") {
					Debug.Assert (bps == -1);
					bps = float.Parse (tokens [1]) / 60;
				} else if (tokens [0] == "OFFSET") {
					Debug.Assert (offset == -1);
					offset = float.Parse (tokens [1]);
				} else if (tokens [0] == "SPEED") {
					multiplier = float.Parse (tokens [1]);
					Debug.Assert (multiplier > 0);
				} else {
					Debug.Assert (bps > 0);
					Debug.Assert (offset >= 0);
					float time = float.Parse (tokens [0]) / bps + offset;
					int id = int.Parse (tokens [1]);
					Debug.Assert (id >= 0 && id < 6);
					BeatCode code = new BeatCode (time, id, multiplier);
					Debug.Assert (code.startTime >= 0);
					beatChart.Enqueue (code);
				}
			}
		}

		Debug.Assert (bps > 0);
		this.offset = (offset == -1) ? 0 : offset;
		this.bps = bps;
		scorer.SetBeatCount (beatChart.Count);    // TODO: beatChart might contain more than just beats in the future
	}

	#if UNITY_EDITOR
	float GetTime (float bpm, float offset, float resolution) {
		return Mathf.Round ((time - offset) * bpm / 60 / resolution) * resolution;
	}

	void OnApplicationQuit () {
		UnityEditor.AssetDatabase.ImportAsset (GENERATED_CHART_PATH);
	}
	#endif
}
