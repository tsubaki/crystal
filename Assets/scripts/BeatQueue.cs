﻿using System.Collections.Generic;

class BeatQueue {
	private LinkedList<BeatCode> list;
	private System.Func<BeatCode, float> key;

	public BeatQueue (System.Func<BeatCode, float> key) {
		list = new LinkedList<BeatCode> ();
		this.key = key;
	} 

	public int Count {
		get {
			return list.Count;
		}
	}

	public void Enqueue (BeatCode code) {
		LinkedListNode<BeatCode> node = list.First;
		if (node == null) {
			list.AddFirst (code);
			return;
		}
		while (key (code) < key (node.Value)) {
			node = node.Next;
			if (node == null) {
				list.AddLast (code);
				return;
			}
		}
		list.AddBefore (node, code);
	}

	public BeatCode Dequeue () {
		AssertNotEmpty ();
		LinkedListNode<BeatCode> node = list.Last;
		list.Remove (node);
		return node.Value;
	}

	public void Clear () {
		list.Clear ();
	}

	public BeatCode Peek () {
		AssertNotEmpty ();
		return list.Last.Value;
	}

	private void AssertNotEmpty () {
		if (Count == 0) {
			throw new System.InvalidOperationException ("BeatQueue is empty.");
		}
	}
}
