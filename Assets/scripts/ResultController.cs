﻿using UnityEngine;
using UnityEngine.UI;

public class ResultController : MonoBehaviour {

	public Text songNameText;
	public Text gradeText;
	public Text scoreText;
	public Text tallyText;

	void OnEnable () {
		Scorer s = Manager.currScorer;

		songNameText.text = Manager.currSongName;
		gradeText.text = s.Grade;
		scoreText.text = s.NormalizedScore.ToString ();
		tallyText.text = string.Format (
			"PERFECT: {0}\n   GREAT: {1}\n    GOOD: {2}\n       BAD: {3}\n      MISS: {4}",
			s.acc0Count, s.acc1Count, s.acc2Count, s.acc3Count, s.missCount);
	}

	void Update () {
		if (Helper.GetAnyButton ()) {
			Manager.StartSong ("Ichido Dake no Koi nara");
			return;
		}
	}
}
