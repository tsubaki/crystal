﻿using UnityEngine;
using System.Collections.Generic;

public class BeatPool {
	private Stack<BeatController> pool;
	private GameObject prefab;
	private Transform crystal;

	public BeatPool (int capacity, GameObject prefab, Transform crystal) {
		pool = new Stack<BeatController> (capacity);
		this.prefab = prefab;
		this.crystal = crystal;
		for (int i = 0; i < capacity; ++i) {
			Add ();
		}
	}

	public BeatPool (GameObject prefab, Transform crystal) : this(20, prefab, crystal) {}

	private void Add () {
		GameObject beat = (GameObject) MonoBehaviour.Instantiate (prefab, Vector3.zero, crystal.rotation);
		beat.SetActive (false);
		pool.Push (beat.GetComponent<BeatController> ());
	}

	public BeatController Borrow () {
		try {
			return pool.Pop ();
		} catch (System.Exception) {
			Add ();
			return pool.Pop ();
		}
	}

	public void Return (BeatController controller) {
		pool.Push (controller);
	}
}
