﻿using UnityEngine.SceneManagement;

class Manager {
	public static string currSongAssetName { get; private set; }
	public static string currSongName { get; private set; }
	public static Scorer currScorer { get; private set; }

	public static void StartSong (string assetName) {
		Manager.currSongAssetName = assetName;
		SceneManager.LoadScene ("main");
	}

	public static void FinishSong (string songName, Scorer scorer) {
		Manager.currSongName = songName;
		Manager.currScorer = scorer;
		SceneManager.LoadScene ("result");
	}
}
