﻿class BeatCode {
	public float time { get; private set; }
	public float startTime { get; private set; }
	public int barId { get; private set; }
	public float speed { get; private set; }

	public BeatController controller;

	public BeatCode (float time, int barId, float speed) {
		this.time = time;
		this.barId = barId;
		this.speed = speed;

		startTime = time - 1 / speed;
	}
}
