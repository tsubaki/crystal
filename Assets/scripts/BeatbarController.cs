﻿using UnityEngine;
using System.Collections;

public class BeatbarController : MonoBehaviour {

	private Animator anim;
	private static int idleHash = Animator.StringToHash ("idle");
	private static int activeHash = Animator.StringToHash ("lightUpBar");

	void Start () {
		anim = GetComponent<Animator> ();
	}

	public void Activate () {
		anim.Play (activeHash);
	}

	public void Deactivate () {
		anim.Play (idleHash);
	}
}	
